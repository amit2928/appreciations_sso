import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom';

export default function Login(props) {

    const navigate = useNavigate();

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token !== null) {
            var y = JSON.parse(token)[0].idTokenClaims
            if (y.aud) {
                const accesstoken_str = sessionStorage.getItem(`msal.token.keys.${y.aud}`);
                const accesstoken_obj = JSON.parse(accesstoken_str)
                // accesstoken_obj.accessToken && navigate(`/dashboard/${accesstoken_obj.accessToken[0]}`)
                if (accesstoken_obj.accessToken && accesstoken_obj.accessToken[0]) {
                    const secret_str = sessionStorage.getItem(accesstoken_obj.accessToken[0]);
                    const secret_obj = JSON.parse(secret_str);
                    secret_obj.secret && navigate(`/dashboard/${secret_obj.secret}`)
                }
            }
        }
    }, [navigate])

    return (
        <div style={{backgroundColor:"#323761"}}></div>
    )
}
