import React, { useState } from 'react';
import './App.css';
import { useMsal, useMsalAuthentication } from '@azure/msal-react';
import { InteractionType } from '@azure/msal-browser';
import { Routes, Route } from "react-router-dom";
import Dashboard from './Dashboard';
import Login from './Login';

function App() {

  useMsalAuthentication(InteractionType.Redirect);
  const [m_strUser, setm_strUser] = useState<string>("");

  function Render() {

    const { accounts } = useMsal();

    try {
      const username = accounts[0].username;
      setm_strUser(username);
      localStorage.setItem("token", JSON.stringify(accounts));
    }
    catch (e) {
    }
  }

  if (m_strUser !== "")
    return (
      <div className="App">
        <Routes>
          <Route path="/" element={<Login />}></Route>
          <Route path='/dashboard/:token' element={<Dashboard />} />
        </Routes>
      </div>
    );
  else
    return <>{Render()}<div style={{ backgroundColor: "#323761" }}></div></>
}

export default App;

